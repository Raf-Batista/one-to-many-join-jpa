package com.example.onetomanyjpa.controller;

import com.example.onetomanyjpa.model.Child;
import com.example.onetomanyjpa.model.Parent;
import com.example.onetomanyjpa.repository.ChildRepository;
import com.example.onetomanyjpa.repository.ParentRepository;
import com.example.onetomanyjpa.service.ChildService;
import com.example.onetomanyjpa.service.ParentService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(FamilyController.class)
public class FamilyControllerTests {
    @Autowired
    MockMvc mockMvc;
    @MockBean
    ParentService parentService;
    @MockBean
    ChildService childService;

    ObjectMapper mapper = new ObjectMapper();

    @DisplayName("This Test Fails Using ObjectMapper")
    @Test
    public void createParentWithManyChildren_withObjectMapper() throws Exception {
        List<Child> children = new ArrayList<>();
        Child a = new Child("Child A");
        Child b = new Child("Child B");
        children.add(a);
        children.add(b);

        Parent parent = new Parent("Parent A", children);

        when(parentService.addChild(any(Parent.class), any(Child.class))).thenReturn(parent);

        mockMvc.perform(post("/parents/1/children")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(parent)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("children", hasSize(2)));
    }
}
