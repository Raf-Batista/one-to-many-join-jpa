package com.example.onetomanyjpa;

import com.example.onetomanyjpa.model.Child;
import com.example.onetomanyjpa.model.Parent;
import com.example.onetomanyjpa.repository.ChildRepository;
import com.example.onetomanyjpa.repository.ParentRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;

// We need to add the "webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)" or else we get an error stating that no beans found for TestRestTemplate

@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
class OneToManyJpaApplicationTests {

    @Autowired
    TestRestTemplate restTemplate;

    @Autowired
    ParentRepository parentRepository;
    @Autowired
    ChildRepository childRepository;

    ObjectMapper mapper = new ObjectMapper();

    @BeforeEach
    public void setUp() {
        List<Child> children = new ArrayList<>();
        Child a = new Child("Child A");
        Child b = new Child("Child B");
        children.add(a);
        children.add(b);

        Parent parent = new Parent("Parent A", children);
        parent.getChildren().forEach(child -> child.setParent(parent));

        parentRepository.save(parent);
    }

    @AfterEach
    public void tearDown() {
        parentRepository.deleteAll();
    }


    @Test
    void contextLoads() {
    }

    @DisplayName("Create Parent with children using ObjectMapper")
    @Test
    void createParent_objectMapper() throws JsonProcessingException {
        String uri = "/parents";

        List<Child> children = new ArrayList<>();
        Child a = new Child("Child A");
        Child b = new Child("Child B");
        children.add(a);
        children.add(b);

        Parent parent = new Parent("Parent A", children);

        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);

        HttpEntity<?> postRequest = new HttpEntity<>(mapper.writeValueAsString(parent), headers);

        ResponseEntity<Parent> response = restTemplate.postForEntity(uri, postRequest, Parent.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assert(Objects.requireNonNull(response.getBody()).getId() > 0);
    }

//    @DisplayName("Create Parent with children using ObjectMapper")
//    @Test
//    void editChildOfParent_objectMapper() throws JsonProcessingException {
//        String uri = "/parents/1/children/1";
//        Child updatedChild = new Child("Child Updated");
//
//        HttpHeaders headers = new HttpHeaders();
//        headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
//
//        HttpEntity<?> patchRequest = new HttpEntity<>(mapper.writeValueAsString(updatedChild), headers);
//
//        ResponseEntity<Parent> response = restTemplate.exchange(uri, HttpMethod.PATCH, patchRequest, Parent.class);
//
//        assertEquals(HttpStatus.OK, response.getStatusCode());
//    }
}
