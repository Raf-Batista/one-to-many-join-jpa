package com.example.onetomanyjpa.controller;

import com.example.onetomanyjpa.model.Child;
import com.example.onetomanyjpa.model.Parent;
import com.example.onetomanyjpa.service.ChildService;
import com.example.onetomanyjpa.service.ParentService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class FamilyController {
    private final ParentService parentService;
    private final ChildService childService;

    public FamilyController(ParentService parentService, ChildService childService) {
        this.parentService = parentService;
        this.childService = childService;
    }

    // GET ALL PARENTS
    @GetMapping("/parents")
    public List<Parent> getAllParents(){
        return parentService.getAllParents();
    }

    // GET ONE PARENT
    @GetMapping("/parents/{parent_id}")
    public Parent getParent(@PathVariable Long parent_id){
        Optional<Parent> parent = parentService.getById(parent_id);
        return parent.orElse(null);
    }

    // GET ALL CHILDREN
    @GetMapping("/children")
    public List<Child> getAllChildren(){
        return childService.getAllChildren();
    }

    // GET ONE CHILD
    @GetMapping("/children/{child_id}")
    public Child getChild(@PathVariable Long child_id){
        Optional<Child> child = childService.getById(child_id);
        return child.orElse(null);
    }

    // CREATE PARENT WITH NO OR MANY CHILDREN
    @PostMapping("/parents")
    public Parent saveParent(@RequestBody Parent parent){
        return parentService.saveParent(parent);
    }

    // ADD CHILD TO ALREADY EXISTING PARENT
    @PostMapping("/parents/{parentId}/children")
    public Parent addChild(@RequestBody Child child, @PathVariable Long parentId) {
        Optional <Parent> optionalParent = parentService.getById(parentId);
        Parent parent = optionalParent.orElse(null);

        if (parent != null) {
            return parentService.addChild(parent, child);
        }
        return null;
    }

    // UPDATE THE PARENT ONLY
    @PatchMapping("/parents/{id}")
    public Parent editParent(@PathVariable Long id, @RequestBody Parent parent) {
        return parentService.updateParent(id, parent);
    }

    // UPDATE THE CHILD OF A PARENT
    @PatchMapping("/parents/{parentId}/children/{childId}")
    public Parent editChild(@PathVariable Long parentId, @PathVariable Long childId, @RequestBody Child child) {
        return parentService.updateChild(parentId, childId, child);
    }

    // DELETE A CHILD FROM A PARENT
    @DeleteMapping("/parents/{parentId}/children/{childId}")
    public ResponseEntity<?> deleteChild(@PathVariable Long parentId, @PathVariable Long childId) {
        parentService.deleteChild(parentId, childId);
        return ResponseEntity.accepted().build();
    }

    // DELETE A PARENT, DELETES ALL CHILDREN FROM THE PARENT
    @DeleteMapping("/parents/{parentId}")
    public ResponseEntity<?> deleteParent(@PathVariable Long parentId) {
        parentService.deleteParent(parentId);
        return ResponseEntity.accepted().build();
    }
}

    /*
        JSON parse error: Cannot construct instance of `com.example.one to many jpa.model.Child` (although at least one Creator exists): no String-argument constructor/factory method to deserialize from String value

        Make sure the JSON being sent is something like this, make sure to model the child exactly like the class

                {
                    "name": "Parent B",
                    "children": [
                          {"name": "Child A"},
                          {"name": "Child B"}
                      ]
                }
    */
