package com.example.onetomanyjpa.repository;

import com.example.onetomanyjpa.model.Child;
import com.example.onetomanyjpa.model.Parent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Optional;

@Repository
public interface ParentRepository extends JpaRepository<Parent, Long> {
}
