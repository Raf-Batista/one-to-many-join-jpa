package com.example.onetomanyjpa.service;

import com.example.onetomanyjpa.model.Child;
import com.example.onetomanyjpa.repository.ChildRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ChildService {
    private final ChildRepository childRepository;

    public ChildService(ChildRepository childRepository) {
        this.childRepository = childRepository;
    }

    public List<Child> getAllChildren() {
        return childRepository.findAll();
    }

    public Optional<Child> getById(Long id) {
        return childRepository.findById(id);
    }
}
