package com.example.onetomanyjpa.service;

import com.example.onetomanyjpa.Exceptions.ChildNotFoundException;
import com.example.onetomanyjpa.model.Child;
import com.example.onetomanyjpa.model.Parent;
import com.example.onetomanyjpa.repository.ChildRepository;
import com.example.onetomanyjpa.repository.ParentRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

@Service
public class ParentService {
    private final ParentRepository parentRepository;
    private final ChildRepository childRepository;

    public ParentService(ParentRepository parentRepository, ChildRepository childRepository) {

        this.parentRepository = parentRepository;
        this.childRepository = childRepository;
    }

    public List<Parent> getAllParents() {
        return parentRepository.findAll();
    }

    public Optional<Parent> getById(Long id) {
        return parentRepository.findById(id);
    }

    public Parent saveParent(Parent parent) {
        parent.getChildren().forEach(child -> child.setParent(parent));
        return parentRepository.save(parent);
    }

    public Parent addChild(Parent parent, Child child) {
        parent.addChild(child);
        return parentRepository.save(parent);
    }

    public Parent updateParent(Long id, Parent updatedParent){
        Optional<Parent> optionalParent = parentRepository.findById(id);
        Parent parentToUpdate = optionalParent.orElse(null);

        if (parentToUpdate != null) {
            parentToUpdate.setName(updatedParent.getName());
            return parentRepository.save(parentToUpdate);
        }

        return null;
    }

    public Parent updateChild(Long parentId, Long childId, Child child) throws ChildNotFoundException {
        Optional<Parent> optionalParent = parentRepository.findById(parentId);
        Parent parent = optionalParent.orElse(null);

        if (parent != null) {
            child.setParent(parent);
            int i = parent.getChildren().indexOf(child);
            parent.getChildren().set(i, child);

            return parentRepository.save(parent);
        }

        throw new ChildNotFoundException("Child not found. Sorry");
    }



//    public Parent updateChild(Long parentId, Long childId, Child child) throws ChildNotFoundException {
//        int index = 0;
//        Optional<Parent> optionalParent = parentRepository.findById(parentId);
//        Parent parent = optionalParent.orElse(null);
//
//        if (parent != null) {
//           List<Child> children = parent.getChildren();
//
//            index = IntStream.range(0, children.size())
//                    .filter(i -> children.get(i).getId().equals(childId))
//                    .findFirst().orElse(-1);
//        }
//
//        if (index >= 0 && parent != null) {
//            List<Child> children = parent.getChildren();
//            Child childToUpdate = children.get(index);
//            childToUpdate.setName(child.getName());
//
//            return parentRepository.save(parent);
//        }
//
//        throw new ChildNotFoundException("Child not found. Sorry");
//    }

    public void deleteChild(Long parentId, Long childId) throws ChildNotFoundException {
        int index = 0;
        Optional<Parent> optionalParent = parentRepository.findById(parentId);
        Parent parent = optionalParent.orElse(null);

        if (parent != null) {
            List<Child> children = parent.getChildren();

            index = IntStream.range(0, children.size())
                    .filter(i -> children.get(i).getId().equals(childId))
                    .findFirst().orElse(-1);
        }

        if (index >= 0 && parent != null) {
            List<Child> children = parent.getChildren();
            Child childToBeRemoved = children.get(index);

            childToBeRemoved.setParent(null);
            children.remove(index);

            childRepository.delete(childToBeRemoved);
            parentRepository.save(parent);

            return;
        }

        throw new ChildNotFoundException("Child not found. Sorry");
    }

    public void deleteParent(Long parentId) {
        Optional<Parent> parent = parentRepository.findById(parentId);

        parent.ifPresent(parentRepository::delete);
    }

}
