package com.example.onetomanyjpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
public class OneToManyJpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(OneToManyJpaApplication.class, args);
    }

}
